Algoritmo OrdenacionSeleccion
	Definir vector Como Entero;
	Dimension vector[10000];
	Definir entrada, long, i, j Como Entero;
	
	Escribir "Introduzca la longitud del Array: ";
	Leer long;
	InicioArray(vector, long);
	VerArray(vector, long);
	Menu;
	Leer entrada;
	
	Mientras entrada <> 0 Hacer
		Segun entrada Hacer
			1:
				Para i <- 1 Hasta (long - 1) Con Paso 1 Hacer
					peq <- i;
					min <- vector[i];
					Para j <- i Hasta long Hacer
						Si vector[j] < min Entonces
							peq <- j;
							min <- vector[j];
						FinSi
					FinPara
					Si vector[peq] <> vector[i] Entonces
						aux <- vector[i];;
						vector[i] <- vector[peq];
						vector[peq] <- aux;
						Escribir " Cambio: ", peq, " por ", i, "  ", Sin Saltar;
						VerArray(vector, long);
					FinSi
				FinPara
				VerArray(vector, long);
			2:
				Escribir "Introduzca la longitud del Array: ";
				Leer long;
				InicioArray(vector, long);
				VerArray(vector, long);
			De Otro Modo:
				Escribir "OPcion incorrecta, Vuelva a introducir la opcion"
		Fin Segun
		
		Menu;
		Leer entrada;
				
	Fin Mientras
	
FinAlgoritmo

SubProceso InicioArray (vectori Por Referencia, longi Por Valor)
	Para i <- 1 Hasta longi Con Paso 1 Hacer
		vectori[i] <- azar(9);
	FinPara
FinSubProceso

SubProceso VerArray (vectorv Por Referencia, longv Por Valor)
	Escribir "Vector: ", Sin Saltar;
	Para i <- 1 Hasta longv Con Paso 1 Hacer
		Escribir vectorv[i], " ", Sin Saltar;
	FinPara
	Escribir " ";
FinSubProceso

Subproceso OrdenaSeleccion (vectoros Por Referencia, longos Por Valor)
	Para j <- longos Hasta 2 Con Paso - 1 Hacer
		ElementoMayor(vectoros, j, may);
		Si vectoros[j] <> vectoros[may] Entonces
			Intercambiar(vectoros, may, j);
			Escribir " Cambio: ", may, " por ", j, "  ", Sin Saltar;
			VerArray(vectoros, longos);
		FinSi
	FinPara
FinSubProceso

SubProceso Intercambiar (vectori Por Referencia, actual Por Valor, siguiente Por Valor)
	Definir aux Como Entero;
	aux <- vectori[actual];
	vectori[actual] <- vectori[siguiente];
	vectori[siguiente] <- aux;
FinSubProceso

SubProceso ElementoMayor (vectore Por Referencia, longe Por Valor, mayore Por Referencia)
	Definir maximo Como Entero;
	mayore <- 1;
	maximo <- vectore[1]
	Para i <- 2 Hasta longe  Con Paso 1 Hacer
		Si vectore[i] > maximo Entonces
			mayore <- i;
			maximo <- vectore[i]
		FinSi
	FinPara
FinSubProceso

Subproceso Menu
	Escribir " ";
	Escribir "Elija una de las siguientes opciones:";
	Escribir "   1 - Ordenar el vector";
	Escribir "   2 - Nuevo vector"; 
	Escribir "   0 - Finalizar"
FinSubProceso

	