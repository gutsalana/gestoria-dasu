Algoritmo OrdenacionShell
	Definir vector Como Entero;
	Dimension vector[10000];
	Definir gap Como Entero;
	Dimension salto[5];
	salto[1] <- 6;
	salto[2] <- 4;
	salto[3] <- 3;
	salto[4] <- 2;
	salto[5] <- 1;
	Definir entrada, long, i, j, k, m Como Entero;
	
	Escribir "Introduzca la longitud del Array: ";
	Leer long;
	InicioArray(vector, long);
	VerArray(vector, long);
	Menu;
	Leer entrada;
	
	Mientras entrada <> 0 Hacer
		Segun entrada Hacer
			1:
				Para i <- 1 Hasta 5 Con Paso 1 Hacer
					gap <- salto[i]
					Para j <- 1 Hasta gap Con Paso 1 Hacer
						Para k <- (gap + j) Hasta long Con Paso gap Hacer
							aux <- vector[k];
							m <- k;
							Mientras m > gap y vector[m-gap] > aux Hacer
								vector[m] <- vector[m-gap]
								m <- m - gap
							FinMientras;
							Si m <> k Entonces
								vector[m] <- aux;
								Escribir "La posicion: ", k, Sin Saltar;
								Escribir " pasa a la posicion: ", m, "  ", Sin Saltar;
								VerArray(vector, long);
							FinSi
						FinPara
					FinPara
				FinPara
				
			2:
				Escribir "Introduzca la longitud del Array: ";
				Leer long;
				InicioArray(vector, long);
				VerArray(vector, long);
			De Otro Modo:
				Escribir "OPcion incorrecta, Vuelva a introducir la opcion"
		Fin Segun
		
		Menu;
		Leer entrada;
		
	Fin Mientras
	
FinAlgoritmo

SubProceso InicioArray (vectori Por Referencia, longi Por Valor)
	Para i <- 1 Hasta longi Con Paso 1 Hacer
		vectori[i] <- azar(longi) + 1;
	FinPara
FinSubProceso

SubProceso VerArray (vectorv Por Referencia, longv Por Valor)
	Escribir "Vector: ", Sin Saltar;
	Para i <- 1 Hasta longv Con Paso 1 Hacer
		Escribir vectorv[i], " ", Sin Saltar;
	FinPara
	Escribir " ";
FinSubProceso

Subproceso Menu
	Escribir " ";
	Escribir "Elija una de las siguientes opciones:";
	Escribir "   1 - Ordenar el vector";
	Escribir "   2 - Nuevo vector"; 
	Escribir "   0 - Finalizar"
FinSubProceso

