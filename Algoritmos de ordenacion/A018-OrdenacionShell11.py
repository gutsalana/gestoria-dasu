# Este codigo ha sido generado por el modulo psexport 20180802-w32 de PSeInt.
# Es posible que el codigo generado no sea completamente correcto. Si encuentra
# errores por favor reportelos en el foro (http://pseint.sourceforge.net).

from random import randint

# En python no hay forma de elegir como pasar una variable a una
# funcion (por referencia o por valor). Las variables "inmutables"
# (str, int, float, bool) se pasan siempre por copia mientras que
# las demas (listas, objetos, etc.) se pasan siempre por referencia.
# Esto coincide con el comportamiento por defecto en pseint, pero
# cuando utiliza las palabras clave "Por Referencia" para
# alterarlo la traducción no es correcta.

def inicioarray(vectori, longi):
	for i in range(1,longi+1):
		vectori[i-1] = randint(0,8)

def verarray(vectorv, longv):
	print("Vector: ",, end="")
	for i in range(1,longv+1):
		print(vectorv[i-1]," ",, end="")
	print(" ")

def menu():
	print(" ")
	print("Elija una de las siguientes opciones:")
	print("   1 - Ordenar el vector")
	print("   2 - Nuevo vector")
	print("   0 - Finalizar")

if __name__ == '__main__':
	vector = int()
	vector = [int() for ind0 in range(10000)]
	gap = int()
	salto = [float() for ind0 in range(5)]
	salto[0] = 6
	salto[1] = 4
	salto[2] = 3
	salto[3] = 2
	salto[4] = 1
	entrada = int()
	long = int()
	i = int()
	j = int()
	k = int()
	m = int()
	print("Introduzca la longitud del Array: ")
	long = int(input())
	inicioarray(vector,long)
	verarray(vector,long)
	MENU()
	entrada = int(input())
	while entrada!=0:
		if entrada==1:
			for i in range(1,6):
				gap = salto[i-1]
				for j in range(1,gap+1):
					for k in range((gap+j),long+1,GAP):
						aux = vector[k-1]
						m = k
						while m>gap and vector[m-gap-1]>aux:
							vector[m-1] = vector[m-gap-1]
							m = m-gap
						if m!=k:
							vector[m-1] = aux
							print("La posicion: ",k,, end="")
							print(" pasa a la posicion: ",m,"  ",, end="")
							verarray(vector,long)
		elif entrada==2:
			print("Introduzca la longitud del Array: ")
			long = int(input())
			inicioarray(vector,long)
			verarray(vector,long)
		else:
			print("OPcion incorrecta, Vuelva a introducir la opcion")
		MENU()
		entrada = int(input())

