Algoritmo OrdenacionBurbuja
	Definir vector Como Entero;
	Dimension vector[10];
	Definir long, aux, i, j Como Entero;
	vector[1] <- 7;
	vector[2] <- 5;
	vector[3] <- 8;
	vector[4] <- 1;
	vector[5] <- 3;
	vector[6] <- 0;
	vector[7] <- 9;
	vector[8] <- 2;
	vector[9] <- 4;
	vector[10] <- 6;
	long <- 10;
	VerArray(vector, long);
	
	Para i <- 1 Hasta long - 1 Con Paso 1 Hacer
		Para j <- 1 Hasta long -1 Con Paso 1 Hacer
			Si vector[j] > vector[j+1] Entonces
				aux <- vector[j];
				vector[j] <- vector[j+1];
				vector[j+1] <- aux;
				Escribir "Pasada: ", i, " Posicion: ", j, "  ", Sin Saltar;
				VerArray(vector, long);
			FinSi
		FinPara
	FinPara
FinAlgoritmo

SubProceso VerArray (vectorv Por Referencia, longv Por Valor)
	Escribir "Vector: ", Sin Saltar;
	Para i <- 1 Hasta longv Con Paso 1 Hacer
		Escribir vectorv[i], " ", Sin Saltar;
	FinPara
	Escribir " ";
FinSubProceso
	