//Torres de hanoi.
Algoritmo LasTorresDeHanoi
	Definir Torres , Altura, ori, aux, des como entero;
	Escribir "Dime la altura";
	Leer Altura;
	Dimension Torres[3, Altura];
	InicioTorres(Torres, Altura);
	VerTorres(Torres, Altura);
	ori<-1;
	aux<-2;
	des<-3;
	Hanoi(Altura, Torres ,ori, aux, des, Altura);
	VerTorres(Torres, Altura);
FinAlgoritmo

SubProceso InicioTorres(LasTorres , Altura)
	Definir I, J como entero;
	para I <- 2 hasta 3 hacer
		para J <- 1 hasta Altura hacer
			LasTorres[I, J] <- 0;
		FinPara
	FinPara
	para J <- 1 hasta Altura hacer
		LasTorres[1, J] <- ( Altura - J) + 1 ;
	FinPara
FinSubProceso

SubProceso VerTorres(LasTorres, Altura)
	Definir I, J como entero;
	para J <- Altura hasta 1 Con Paso -1 hacer
		para I <- 1 hasta 3 hacer
			Escribir LasTorres[I, J] ," " Sin Saltar ;
		FinPara
		Escribir "";
	FinPara
	Escribir "------------------------------";
FinSubProceso

SubProceso Pasar(LasTorres,origen, destino ,Altura)
	Definir aux, io, id Como Entero;
	io <- Altura ; //bucar el ultimo cero
	mientras io > 1 & LasTorres[origen, io ] = 0
		io <- io - 1;
	FinMientras
	id <- Altura + 1;
	mientras id > 1 & LasTorres[destino, id - 1] = 0
		id <- id - 1;
	FinMientras
	Escribir "pasar de origen ", origen, " destino ", destino;
	LasTorres[destino, id ] <- LasTorres[origen, io];
	LasTorres[origen, io]<-0;
	VerTorres(LasTorres, Altura);
FinSubProceso

SubProceso Hanoi(Alt, LasTorres,origen, auxiliar, destino, Altura)
	Si Alt = 1 entonces
		//el disco 1 de pila origen a la pila destino
		//(insertarlo arriba de la pila destino)
		Pasar(LasTorres,origen, destino ,Altura);
	Sino
		//mover todas las fichas menos la m�s grande (n)
		//a la varilla auxiliar
		hanoi(Alt - 1, LasTorres, origen, destino, auxiliar , altura);
		//mover la ficha grande hasta la varilla final
		Pasar(LasTorres,origen, destino ,Altura);
		//mover todas las fichas restantes,
		//0...n-1, encima de la ficha grande (n)
		hanoi(Alt - 1 , LasTorres, auxiliar, origen, destino, altura);
	FinSi
FinSubProceso
