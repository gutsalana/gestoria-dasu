Algoritmo OrdenacionBurbuja
	Definir vector Como Entero;
	Dimension vector[10];
	Definir long, aux, i, j Como Entero;
	long <- 10;
	
	InicioArray(vector, long);
	VerArray(vector, long);
	
	Para i <- 1 Hasta long - 1 Con Paso 1 Hacer
		Para j <- 1 Hasta long -1 Con Paso 1 Hacer
			Si vector[j] > vector[j+1] Entonces
				aux <- vector[j];
				vector[j] <- vector[j+1];
				vector[j+1] <- aux;
				Escribir "Pasada: ", i, " Posicion: ", j, "  ", Sin Saltar;
				VerArray(vector, long);
			FinSi
		FinPara
	FinPara
FinAlgoritmo

SubProceso InicioArray (vectori Por Referencia, longi Por Valor)
	Para i <- 1 Hasta longi Con Paso 1 Hacer
		vectori[i] <- azar(9);
	FinPara
FinSubProceso

SubProceso VerArray (vectorv Por Referencia, longv Por Valor)
	Escribir "Vector: ", Sin Saltar;
	Para i <- 1 Hasta longv Con Paso 1 Hacer
		Escribir vectorv[i], " ", Sin Saltar;
	FinPara
	Escribir " ";
FinSubProceso
	