Algoritmo OrdenacionBurbujaMejorado
	Definir vector Como Entero;
	Dimension vector[10000];
	Definir entrada, long, i, j Como Entero;
	
	Escribir "Introduzca la longitud del Array: ";
	Leer long;
	InicioArray(vector, long);
	VerArray(vector, long);
	Menu;
	Leer entrada;
	
	Mientras entrada <> 0 Hacer
		Segun entrada Hacer
			1:
				Para j <- long Hasta 2 Con Paso - 1 Hacer
					ElementoMayor(vector, j, may);
					Si vector[j] <> vector[may] Entonces
						Intercambiar(vector, may, j);
						Escribir " Cambio: ", may, " por ", j, "  ", Sin Saltar;
						VerArray(vector, long);
					FinSi
				FinPara
			2:
				Escribir "Introduzca la longitud del Array: ";
				Leer long;
				InicioArray(vector, long);
				VerArray(vector, long);
			De Otro Modo:
				Escribir "OPcion incorrecta, Vuelva a introducir la opcion"
		Fin Segun
		
		Menu;
		Leer entrada;
				
	Fin Mientras
	
FinAlgoritmo

SubProceso InicioArray (vectori Por Referencia, longi Por Valor)
	Para i <- 1 Hasta longi Con Paso 1 Hacer
		vectori[i] <- azar(9);
	FinPara
FinSubProceso

SubProceso VerArray (vectorv Por Referencia, longv Por Valor)
	Escribir "Vector: ", Sin Saltar;
	Para i <- 1 Hasta longv Con Paso 1 Hacer
		Escribir vectorv[i], " ", Sin Saltar;
	FinPara
	Escribir " ";
FinSubProceso

SubProceso Intercambiar (vectori Por Referencia, actual Por Valor, siguiente Por Valor)
	Definir aux Como Entero;
	aux <- vectori[actual];
	vectori[actual] <- vectori[siguiente];
	vectori[siguiente] <- aux;
FinSubProceso

SubProceso ElementoMayor (vectore Por Referencia, longe Por Valor, mayore Por Referencia)
	Definir maximo Como Entero;
	mayore <- 1;
	maximo <- vectore[1]
	Para i <- 2 Hasta longe  Con Paso 1 Hacer
		Si vectore[i] > maximo Entonces
			mayore <- i;
			maximo <- vectore[i]
		FinSi
	FinPara
FinSubProceso

Subproceso Menu
	Escribir " ";
	Escribir "Elija una de las siguientes opciones:";
	Escribir "   1 - Ordenar el vector";
	Escribir "   2 - Nuevo vector"; 
	Escribir "   0 - Finalizar"
FinSubProceso

	