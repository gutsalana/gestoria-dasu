Algoritmo OrdenacionBurbujaMejorado
	Definir vector Como Entero;
	Dimension vector[10000];
	Definir entrada, long, i, j Como Entero;
	
	Escribir "Introduzca la longitud del Array: ";
	Leer long;
	InicioArray(vector, long);
	VerArray(vector, long);
	Menu;
	Leer entrada;
	
	Mientras entrada <> 0 Hacer
		Segun entrada Hacer
			1:
				i <- 1
				Repetir
					//Escribir "Pasada: ", i;
					nointercambio <- Verdadero;
					Para j <- 1 Hasta long - 1 Con Paso 1 Hacer
						Si vector[j] > vector[j+1] Entonces
							Intercambiar(vector, j, j+1);
							//Escribir " Posicion: ", j, "  ", Sin Saltar;
							//VerArray(vector, long);
							nointercambio <- Falso;
						FinSi
					FinPara
					i <- i + 1;
				Hasta Que nointercambio 
				VerArray(vector, long);
			2:
				Escribir "Introduzca la longitud del Array: ";
				Leer long;
				InicioArray(vector, long);
				VerArray(vector, long);
			De Otro Modo:
				Escribir "OPcion incorrecta, Vuelva a introducir la opcion"
		Fin Segun
		
		Menu;
		Leer entrada;
				
	Fin Mientras
	
FinAlgoritmo

SubProceso InicioArray (vectori Por Referencia, longi Por Valor)
	Para i <- 1 Hasta longi Con Paso 1 Hacer
		vectori[i] <- azar(9);
	FinPara
FinSubProceso

SubProceso VerArray (vectorv Por Referencia, longv Por Valor)
	Escribir "Vector: ", Sin Saltar;
	Para i <- 1 Hasta longv Con Paso 1 Hacer
		Escribir vectorv[i], " ", Sin Saltar;
	FinPara
	Escribir " ";
FinSubProceso

SubProceso Intercambiar (vectori Por Referencia, actual Por Valor, siguiente Por Valor)
	Definir aux Como Entero;
	aux <- vectori[actual];
	vectori[actual] <- vectori[siguiente];
	vectori[siguiente] <- aux;
FinSubProceso

Subproceso Menu
	Escribir " ";
	Escribir "Elija una de las siguientes opciones:";
	Escribir "   1 - Ordenar el vector";
	Escribir "   2 - Nuevo vector"; 
	Escribir "   0 - Finalizar"
FinSubProceso

	