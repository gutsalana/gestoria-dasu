#Programa que cambia año de decimal a romanos y viceversa
print('Programa que cambia año de decimal a romanos y viceversa')
print(' ')

lista_buena = ["M","D","C","L","X","V","I"]
matriz = [["M","D","C","L","X","V","I"],[1000,500,100,50,10,5,1]]
anio_rom = []
anio_rom_sal = []

i = int
j = int
opcion = int
anio_dec = int
numero = int
num = 1

numero = 1
print('Si quiere cambiar un anio en decimal a romano escriba   1')
print('Si quiere cambiar un anio en romano a decimal escriba   2')
opcion = int(input('Elija opcion: '))
print(' ')

while opcion < 1 or opcion > 2:
    print('Opcion incorrecta')
    print(' ')
    print('Si quiere cambiar un anio en decimal a romano escriba   1')
    print('Si quiere cambiar un anio en romano a decimal escriba   2')
    opcion = int(input('Elija opcion: '))
    print(' ')

if opcion == 1:
    anio_dec = int(input('Introduzca el año entre 0 y 3999: '))
    while anio_dec < 0 or anio_dec > 3999:
        print('Año incorrecto')
        anio_dec = int(input('Introduzca el año entre 0 y 3999:'))
    numero = anio_dec
    for j in range(7):
        while numero >= matriz[1][j]:
#            print(matriz[0][j])
            anio_rom.append(matriz[0][j])
            numero = numero - matriz[1][j]
    longitud = len(anio_rom)
    i = 0
    while i < longitud:
        num = num + 1
        if anio_rom[i] == "M":
            anio_rom_sal.append("M")
            i = i + 1
        elif anio_rom[i] == "D":
            if anio_rom[i+1] == "C" and anio_rom[i+2] == "C" and anio_rom[i+3] == "C" and anio_rom[i+4] == "C":
                anio_rom_sal.append("C")
                anio_rom_sal.append("M")
                i = i + 5
            else:
                anio_rom_sal.append("D")
                i = i + 1
        elif anio_rom[i] == "C":
            if anio_rom[i+1] == "C" and anio_rom[i+2] == "C" and anio_rom[i+3] == "C":
                anio_rom_sal.append("C")
                anio_rom_sal.append("D")
                i = i + 4
            else:
                anio_rom_sal.append("C")
                i = i + 1
        elif anio_rom[i] == "L":
            if anio_rom[i+1] == "X" and anio_rom[i+2] == "X" and anio_rom[i+3] == "X" and anio_rom[i+4] == "X":
                anio_rom_sal.append("X")
                anio_rom_sal.append("C")
                i = i + 5
            else:
                anio_rom_sal.append("L")
                i = i + 1
        elif anio_rom[i] == "X":
            if anio_rom[i+1] == "X" and anio_rom[i+2] == "X" and anio_rom[i+3] == "X":
                anio_rom_sal.append("X")
                anio_rom_sal.append("L")
                i = i + 4
            else:
                anio_rom_sal.append("X")
                i = i + 1
        elif anio_rom[i] == "V":
            if anio_rom[i+1] == "I" and anio_rom[i+2] == "I" and anio_rom[i+3] == "I" and anio_rom[i+4] == "I":
                anio_rom_sal.append("I")
                anio_rom_sal.append("X")
                i = i + 5
            else:
                anio_rom_sal.append("V")
                i = i + 1
        elif anio_rom[i] == "I":
            if anio_rom[i+1] == "I" and anio_rom[i+2] == "I" and anio_rom[i+3] == "I":
                anio_rom_sal.append("I")
                anio_rom_sal.append("V")
                i = i + 4
            else:
                anio_rom_sal.append("I")
                i = i + 1
    print('El año en decimal: ', anio_dec, ' - Corresponde a año romano: ', end="")
    
    longitud = len(anio_rom_sal)
    for i in range(longitud):
        print(anio_rom_sal[i], end="")
    print(' ')
    
elif opcion == 2:
    anio_incorrecto = True
    while anio_incorrecto:
        anio_rom_ent = str(input('Introduzca el año en nuemros romanos: '))
        anio_rom_ent = anio_rom_ent.upper()
        anio_rom = list(anio_rom_ent)
        print(anio_rom)
        longitud = len(anio_rom)
        anio_incorrecto = False
        for i in range(longitud):
            if anio_rom[i] not in lista_buena:
                anio_incorrecto = True
        if anio_incorrecto:
            print('El año romano introducido es incorrecto 1')
        if not anio_incorrecto:
            hay_M = False
            hay_D = False
            hay_C = False
            hay_L = False
            hay_X = False
            hay_V = False
            hay_I = False
            for i in range(longitud):
                if anio_rom[i] == "M":
                    hay_M = True
                    if hay_D or hay_L or hay_X or hay_V or hay_I:
                        anio_incorrecto = True
                        print('Año romano incorrecto - M precedida de D o L o X o V o I. ')
                    if i > 2:
                        if (anio_rom[i-1] == "C") and (anio_rom[i-2] == "M") and (anio_rom[i-3] == "C"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - CMCM. ')
                        if (anio_rom[i-1] == "M") and (anio_rom[i-2] == "M") and (anio_rom[i-3] == "M"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - MMMM. ')
                    if i > 1:
                        if (anio_rom[i-1] == "M") and (anio_rom[i-2] == "C"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - CMM. ')
                    if i > 1:
                        if (anio_rom[i-1] == "C") and (anio_rom[i-2] == "C"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - CCM. ')
                        
                elif anio_rom[i] == "D":
                    hay_D = True
                    if hay_L or hay_X or hay_V or hay_I:
                        anio_incorrecto = True
                        print('Año romano incorrecto - D precedida de L o X o V o I. ')
                    if i > 2:
                        if (anio_rom[i-1] == "C") and (anio_rom[i-2] == "M") and (anio_rom[i-3] == "C"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - CMCD. ')
                        if (anio_rom[i-1] == "C") and (anio_rom[i-2] == "D") and (anio_rom[i-3] == "C"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - CDCD. ')
                    if i > 1:
                        if (anio_rom[i-1] == "C") and (anio_rom[i-2] == "C"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - CCD. ')
                    if i > 0:
                        if (anio_rom[i-1] == "D"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - DD. ')
                        
                elif anio_rom[i] == "C":
                    hay_C = True
                    if hay_L or hay_V or hay_I:
                        anio_incorrecto = True
                        print('Año romano incorrecto - C precedida de L o V o I. ')
                    if i > 2:
                        if (anio_rom[i-1] == "X") and (anio_rom[i-2] == "C") and (anio_rom[i-3] == "X"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - XCXC. ')
                        if (anio_rom[i-1] == "C") and (anio_rom[i-2] == "C") and (anio_rom[i-3] == "C"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - CCCC. ')
                    if i > 1:
                        if (anio_rom[i-1] == "M") and (anio_rom[i-2] == "C"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - CMC. ')
                        if (anio_rom[i-1] == "D") and (anio_rom[i-2] == "C"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - CDC. ')
                        if (anio_rom[i-1] == "X") and (anio_rom[i-2] == "X"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - XXC. ')
                        
                elif anio_rom[i] == "L":
                    hay_L = True
                    if hay_V or hay_I:
                        anio_incorrecto = True
                        print('Año romano incorrecto - L precedida de V o I. ')
                    if i > 2:
                        if (anio_rom[i-1] == "X") and (anio_rom[i-2] == "C") and (anio_rom[i-3] == "X"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - XCXL. ')
                        if (anio_rom[i-1] == "X") and (anio_rom[i-2] == "L") and (anio_rom[i-3] == "X"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - XLXL. ')
                    if i > 1:
                        if (anio_rom[i-1] == "X") and (anio_rom[i-2] == "X"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - XXL. ')
                    if i > 0:
                        if (anio_rom[i-1] == "L"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - LL. ')
                            
                elif anio_rom[i] == "X":
                    hay_X = True
                    if hay_V:
                        anio_incorrecto = True
                        print('Año romano incorrecto - X precedida de V. ')
                    if i > 2:
                        if (anio_rom[i-1] == "I") and (anio_rom[i-2] == "X") and (anio_rom[i-3] == "I"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - IXIX. ')
                        if (anio_rom[i-1] == "X") and (anio_rom[i-2] == "X") and (anio_rom[i-3] == "X"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - XXXX. ')
                    if i > 1:
                        if (anio_rom[i-1] == "C") and (anio_rom[i-2] == "X"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - XCX. ')
                        if (anio_rom[i-1] == "L") and (anio_rom[i-2] == "X"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - XLX. ')
                        if (anio_rom[i-1] == "I") and (anio_rom[i-2] == "I"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - IIX. ')
                            
                elif anio_rom[i] == "V":
                    hay_V = True
                    if i > 2:
                        if (anio_rom[i-1] == "I") and (anio_rom[i-2] == "X") and (anio_rom[i-3] == "I"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - IXIV. ')
                        if (anio_rom[i-1] == "I") and (anio_rom[i-2] == "V") and (anio_rom[i-3] == "I"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - IVIV. ')
                    if i > 1:
                        if (anio_rom[i-1] == "I") and (anio_rom[i-2] == "I"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - IIV. ')
                    if i > 0:
                        if (anio_rom[i-1] == "V"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - VV. ')
                            
                elif anio_rom[i] == "I":
                    hay_I = True
                    if i > 2:
                        if (anio_rom[i-1] == "I") and (anio_rom[i-2] == "I") and (anio_rom[i-3] == "I"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - IIII. ')
                    if i > 1:
                        if (anio_rom[i-1] == "X") and (anio_rom[i-2] == "I"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - IXI. ')
                        if (anio_rom[i-1] == "V") and (anio_rom[i-2] == "I"):
                            anio_incorrecto = True
                            print('Año romano incorrecto - IVI. ')

    anio_dec = 0
    valor = 0
    valor_ant = 0
    for i in range(longitud):
        for j in range(7):
            if anio_rom[i] == matriz[0][j]:
                valor = matriz[1][j]
                print(valor)
                anio_dec = anio_dec + valor
                if valor_ant < valor:
                    anio_dec = anio_dec - (valor_ant * 2)
                valor_ant = valor

    print('El año en romano: ', end="")
    for i in range(longitud):
        print(anio_rom[i], end="")
    print(' - Corresponde al año decimal: ', anio_dec)

final = str(input('Pulse una tecla para terminar'))

