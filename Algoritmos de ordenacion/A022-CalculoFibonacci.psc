// Implementaci�n del c�lculo del elemento de la serie 
// de Fibonaccimediante una funci�n recursiva
// El paso recursivo se basa en que An = (An-1) + (An-2)
// El paso base se base en que A0 = 1 y A2 = 1

Funcion fibo <- Fibonacci (n)
    Si (n = 1) o (n = 2) Entonces
        fibo <- 1;
    sino 
        fibo <- Fibonacci(n - 1) + Fibonacci(n - 2); 
    FinSi
FinFuncion

Algoritmo CalculoFibonacci
    Escribir "Ingrese El numero del elemento"
    Leer num
	Escribir "El elemento ", num, " de la serie es: ", Fibonacci(num);
FinAlgoritmo
