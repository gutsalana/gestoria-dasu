// Implementaci�n del c�lculo del factorial mediante una funci�n recursiva
// El paso recursivo se basa en que N! = N * (N -1)!
// El paso base se base en que 0! = 1

Funcion resultado <- Factorial (n)
    Si n = 0 Entonces
        resultado <- 1;
    sino 
        resultado <- n * Factorial(n - 1); 
    FinSi
FinFuncion

Algoritmo CalculoFactorial
    Escribir "Ingrese El numero"
    Leer base
    Escribir "El resultado es ", Factorial(base)
FinAlgoritmo
