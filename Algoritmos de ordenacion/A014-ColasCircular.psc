Funcion vacia <- ColaVacia (colav, frentev, finalv, numelev)
	Si numelev = 0 Entonces
		vacia <- Verdadero;
	SiNo
		vacia <- Falso;
	FinSi
Fin Funcion

Funcion llena <- ColaLlena (colal, numelel, maximol)
	Si numelel = maximol Entonces
		llena <- Verdadero;
	SiNo
		llena <- Falso;
	FinSi
Fin Funcion

Algoritmo colas
	//Definicion de estructura tipo cola
	Definir MAXCOLA Como Entero;
	MAXCOLA <- 5;
	Definir frente Como Entero;
	Definir final Como Entero;
	Definir numele Como Entero;
	Definir cola Como Caracter;
	Dimension cola[MAXCOLA];
	//Fin definicion de cola
	
	Definir vacia Como Logico;
	Definir llena Como Logico;
	Definir exito Como Logico;
	Definir elemento Como Caracter;
	Definir entrada Como Entero;
	Definir i, f Como Entero;
	numele <- 0;
	frente <- 0;
	final <- 1;
	
	Inicializarcola(cola, frente, final, numele);
	
	Menu;
	Leer entrada;
	Mientras entrada <> 0 Hacer
		Segun entrada Hacer
			1:
				Escribir "Introduce elemento: "
				Leer elemento;
				Metercola(cola, frente, final, numele, elemento, exito, MAXCOLA)
				Si exito Entonces
					Escribir "El elemento ", elemento, " esta acolado en la posicion ", numele;
				SiNo
					Escribir "cola LLENA. el elemento ", elemento, " no se ha introducido";
				FinSi
				Escribir "frente = ", frente, "   final = ", final, "   numele = ", numele;
			2:
				Sacarcola(cola, frente, final, numele, elemento, exito, MAXCOLA)
				Si exito Entonces
					Escribir "El elemento sacado es: ", elemento;
				SiNo
					Escribir "cola VACIA. No existe ningun elemento";
				FinSi
				Escribir "frente = ", frente, "   final = ", final, "   numele = ", numele;
			3:
				Recuperarcola(cola, frente, final, numele, elemento, exito)
				Si exito Entonces
					Escribir "El elemento de la posicion ", frente - 1, " es: ", elemento;
				SiNo
					Escribir "cola VACIA. No existe ningun elemento";
				FinSi
			4:
				Escribir "La cola tiene los siguientes elementos"
				f <- frente;
				Escribir "numele = ", numele;
				Para i <- 1 Hasta numele Con Paso 1 Hacer
					Escribir "i = ", i, "  f = ", f;
					Escribir "El elemento ", i, " es ", cola[f];
					f <- f + 1;
					Si (f = MAXCOLA + 1) Entonces
						f <- 1;
					FinSi
				FinPara
			5:
				Escribir "Palabra de entrada y de salida: ", Sin Saltar;
				f <- frente;
				Para i <- 1 Hasta numele Con Paso 1 Hacer
					Escribir cola[f], Sin Saltar;
					f <- f + 1;
					Si (f = MAXCOLA + 1) Entonces
						f <- 1;
					FinSi
				FinPara
				Escribir " ";
			6:
				Inicializarcola(cola, frente, final, numele);
				Escribir "cola inicializada";
			De Otro Modo:
				Escribir "OPcion incorrecta, Vuelva a introducir la opcion"
		Fin Segun
		
		Menu;
		Leer entrada;
		Escribir " ";
		
	Fin Mientras
	
	Escribir "FIN PROGRAMA"
		
FinAlgoritmo

SubProceso Inicializarcola (colai Por Referencia, frentei Por referencia, finali Por Referencia, numelei Por Referencia)
	frentei <- 1;
	finali <- 1;
	numelei <- 0;
FinSubProceso

SubProceso Metercola (colam Por Referencia, frentem Por referencia, finalm Por referencia, numelem Por referencia, elem Por Valor, exitom Por Referencia, maximom Por Valor)
	Si ColaLlena(colam, numelem, maximom) Entonces 
		exitom <- Falso;
	SiNo
		colam[finalm] <- elem;
		finalm = finalm + 1;
		Si (finalm = maximom + 1) Entonces
			finalm <- 1;
		FinSi
		numelem = numelem + 1;
		exitom <- Verdadero;
	FinSi
FinSubProceso

SubProceso Sacarcola (colasa Por Referencia, frentesa Por referencia, finalsa Por referencia, numelesa Por referencia, elesa Por Referencia, exitosa Por Referencia, maximosa Por Valor)
	Si ColaVacia(colasa, frentesa, finalsa, numelesa) Entonces 
		exitosa <- Falso;
	SiNo
		elesa <- colasa[frentesa]
		numelesa = numelesa - 1;
		Si numelesa = 0 Entonces
			Inicializarcola(colasa, frentesa, finalsa, numelesa);
		SiNo
			frentesa <- frentesa + 1;
			Si (frentesa = maximosa + 1) Entonces
				frentesa <- 1;
			FinSi
		FinSi
		exitosa <- Verdadero;
	FinSi
FinSubProceso

SubProceso Recuperarcola (colar Por Referencia, frenter Por Valor, finalr Por Valor, numeler Por Valor, eler Por Referencia, exitor Por Referencia)
	Si ColaVacia(colasa, frentesa, finalsa, numelesa) Entonces 
		exitor <- Falso;
	SiNo
		eler <- colar[frenter]
		exitor <- Verdadero;
	FinSi
FinSubProceso

Subproceso Menu
	Escribir " ";
	Escribir "Elija una de las siguientes opciones:";
	Escribir "   1 - Introducir elemento en la cola";
	Escribir "   2 - Sacar elemento de la cola"; 
	Escribir "   3 - Consultar elemento de la cola";
	Escribir "   4 - Visualizar los elementos de la cola";
	Escribir "   5 - Visualizar la palabra de entrada";
	Escribir "   6 - Inicializar la cola";
	Escribir "   0 - Finalizar"
FinSubProceso

