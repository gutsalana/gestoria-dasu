Algoritmo OrdenacionBurbujaMejorado
	Definir vector Como Entero;
	Dimension vector[10000];
	Definir entrada, long, i, j Como Entero;
	
	Escribir "Introduzca la longitud del Array: ";
	Leer long;
	InicioArray(vector, long);
	VerArray(vector, long);
	Menu;
	Leer entrada;
	
	Mientras entrada <> 0 Hacer
		Segun entrada Hacer
			1:
				Para j <- long Hasta 2 Con Paso - 1 Hacer
					ElementoMayor(vector, j, may);
					Si vector[j] <> vector[may] Entonces
						Intercambiar(vector, may, j);
						Escribir " Cambio: ", may, " por ", j, "  ", Sin Saltar;
						VerArray(vector, long);
					FinSi
				FinPara
			2:
				Escribir "Introduzca la longitud del Array: ";
				Leer long;
				InicioArray(vector, long);
				VerArray(vector, long);
			De Otro Modo:
				Escribir "OPcion incorrecta, Vuelva a introducir la opcion"
		Fin Segun
		
		Menu;
		Leer entrada;
				
	Fin Mientras
	
FinAlgoritmo

SubProceso InicioArray (vectori Por Referencia, longi Por Valor)
	Para i <- 1 Hasta longi Con Paso 1 Hacer
		vectori[i] <- azar(9);
	FinPara
FinSubProceso

SubProceso OrdenaSeleccion (vectoros Por Referencia, longos Por Valor, 
	Para j <- long Hasta 2 Con Paso - 1 Hacer
		ElementoMayor(vectoros, j, may);
		Si vectoros[j] <> vectoros[may] Entonces
			Intercambiar(vectoros, may, j);
			Escribir " Cambio: ", may, " por ", j, "  ", Sin Saltar;
			VerArray(vectoros, longos);
		FinSi
	FinPara
FinSubProceso

SubProceso VerArray (vectorv Por Referencia, longv Por Valor)
	Escribir "Vector: ", Sin Saltar;
	Para i <- 1 Hasta longv Con Paso 1 Hacer
		Escribir vectorv[i], " ", Sin Saltar;
	FinPara
	Escribir " ";
FinSubProceso

SubProceso Intercambiar (vectori Por Referencia, actual Por Valor, siguiente Por Valor)
	Definir aux Como Entero;
	aux <- vectori[actual];
	vectori[actual] <- vectori[siguiente];
	vectori[siguiente] <- aux;
FinSubProceso

SubProceso ElementoMayor (vectore Por Referencia, longe Por Valor, mayore Por Referencia)
	Definir maximo Como Entero;
	mayore <- 1;
	maximo <- vectore[1]
	Para i <- 2 Hasta longe  Con Paso 1 Hacer
		Si vectore[i] > maximo Entonces
			mayore <- i;
			maximo <- vectore[i]
		FinSi
	FinPara
FinSubProceso

Subproceso tri_Insertion ( vectorti Por Referencia, longti Por Valor, gapti Por Valor, debutti Por Valor)
    Para i <- debutti Hasta longti Con Paso gapti Hacer
		mayor
	FinPara
	POUR i VARIANT DE debut A n AVEC UN PAS gap FAIRE
		INSERER a[i] � sa place dans a[1:i-1];
FinSubProceso;

Subproceso tri_shell ( vectorts, longts)
	Definir gap Como Entero;
	Dimension gap[5];
	gap[1] <- 6;
	gap[2] <- 4;
	gap[3] <- 3;
	gap[4] <- 2;
	gap[5] <- 1;
	Para i <- 1 Hasta 5 Con Paso 1 Hacer
		Para debut <- 0 Hasta gap[i] Con Paso - 1 Hacer
			tri_Insertion(vectorts, longts, gap[i], debut);
		FinPara
	FinPara
    //POUR gap DANS (6,4,3,2,1) FAIRE
	//	POUR debut VARIANT DE 0 A gap - 1 FAIRE
	//		tri_Insertion(Tableau,gap,debut);
	//	FIN POUR;
	//FIN POUR;
FinSubProceso;

Subproceso Menu
	Escribir " ";
	Escribir "Elija una de las siguientes opciones:";
	Escribir "   1 - Ordenar el vector";
	Escribir "   2 - Nuevo vector"; 
	Escribir "   0 - Finalizar"
FinSubProceso

	