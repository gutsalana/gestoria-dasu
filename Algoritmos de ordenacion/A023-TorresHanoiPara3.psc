// Implementación Torres Hanoi mediante una función recursiva
// El paso recursivo se basa en que 
// El paso base se base en que 

Algoritmo TorresHanoi
    MoverTorre(3,"A","B","C");
FinAlgoritmo

SubProceso MoverTorre (altura, origen, destino, intermedio)
	Si altura >= 1 Entonces
		MoverTorre(altura-1, origen, intermedio, destino)
		MoverDisco(origen, destino)
		MoverTorre(altura-1, intermedio, destino, origen)
	FinSi
FinSubProceso

SubProceso MoverDisco (inicio, hacia)
	Escribir "mover disco de ", inicio," a ", hacia;
FinSubProceso
