Funcion vacia <- PilaVacia (pilav, cimav)
	Si cimav = 0 Entonces
		vacia <- Verdadero;
	SiNo
		vacia <- Falso;
	FinSi
Fin Funcion

Funcion llena <- Pilallena (pilal, cimal, maximo)
	Si cimal = maximo Entonces
		llena <- Verdadero;
	SiNo
		llena <- Falso;
	FinSi
Fin Funcion

Algoritmo Pilas
	//Definicion de estructura tipo pila
	Definir MAXPILA Como Entero;
	MAXPILA <- 5;
	Definir cima Como Entero;
	Definir pila Como Caracter;
	Dimension pila[MAXPILA];
	//Fin definicion de pila
	
	Definir vacia Como Logico;
	Definir llena Como Logico;
	Definir exito Como Logico;
	Definir elemento Como Caracter;
	Definir entrada Como Entero;
	
	InicializarPila(pila, cima);
	
	Menu;
	Leer entrada;
	Mientras entrada <> 0 Hacer
		Segun entrada Hacer
			1:
				Escribir "Introduce elemento: "
				Leer elemento;
				MeterPila(pila, cima, elemento, exito, MAXPILA)
				Si exito Entonces
					Escribir "El elemento ", elemento, " esta apilado en la posicion ", cima;
				SiNo
					Escribir "PILA LLENA. el elemento ", elemento, " no se ha introducido";
				FinSi
			2:
				SacarPila(pila, cima, elemento, exito)
				Si exito Entonces
					Escribir "El elemento sacado de la posicion ", cima + 1, " es: ", elemento;
				SiNo
					Escribir "PILA VACIA. No existe el elemento de la posicion ", cima;
				FinSi
			3:
				RecuperarPila(pila, cima, elemento, exito)
				Si exito Entonces
					Escribir "El elemento de la posicion ", cima, " es: ", elemento;
				SiNo
					Escribir "PILA VACIA. No existe el elemento de la posicion ", cima;
				FinSi
			4:
				Escribir "La pila tiene los siguientes elementos segun entrada"
				Para i <- 1 Hasta cima Con Paso 1 Hacer
					Escribir "El elemento ", i, " es ", pila[i];
				FinPara
			5:
				Escribir "La pila tiene los siguientes elementos segun salida"
				Para i <- cima Hasta 1 Con Paso -1 Hacer
					Escribir "El elemento ", i, " es ", pila[i];
				FinPara
			6:
				InicializarPila(pila, cima);
				Escribir "Pila inicializada";
			De Otro Modo:
				Escribir "OPcion incorrecta, Vuelva a introducir la opcion"
		Fin Segun
		
		Menu;
		Leer entrada;
		Escribir " ";
		
	Fin Mientras
	
	Escribir "FIN PROGRAMA"
	//Escribir "cima = ", cima;
	//Escribir "PilaVacia = ", PilaVacia(pila, cima);
	//Escribir "PilaLlena = ", PilaLlena(pila, cima, MAXPILA);
	//Si PilaVacia(pila, cima) Entonces
	//	Escribir "La Pila esta vacia"
	//SiNo
	//	Escribir "La Pila no esta vacia"
	//FinSi
	
	//Si Pilallena(pila, cima, MAXPILA) Entonces
	//	Escribir "La Pila esta llena"
	//SiNo
	//	Escribir "La Pila no esta llena"
	//FinSi
	
	//Escribir "Introduce elemento: "
	//Leer elemento;
	//MeterPila(pila, cima, elemento, exito, MAXPILA)
	//Si exito Entonces
	//	Escribir "El elemento ", elemento, " esta apilado en la posicion ", cima;
	//SiNo
	//	Escribir "PILA LLENA. el elemento ", elemento, " no se ha introducido";
	//FinSi
	
	//RecuperarPila(pila, cima, elemento, exito)
	//Si exito Entonces
	//	Escribir "El elemento de la posicion ", cima, " es: ", elemento;
	//SiNo
	//	Escribir "PILA VACIA. No existe el elemento de la posicion ", cima;
	//FinSi
	
	//SacarPila(pila, cima, elemento, exito)
	//Si exito Entonces
	//	Escribir "El elemento sacado de la posicion ", cima + 1, " es: ", elemento;
	//SiNo
	//	Escribir "PILA VACIA. No existe el elemento de la posicion ", cima;
	//FinSi
	
FinAlgoritmo

SubProceso InicializarPila (pilai Por Referencia, cimai Por referencia)
	cimai <- 0;
FinSubProceso

SubProceso MeterPila (pilam Por Referencia, cimam Por referencia, elem Por Valor, exitom Por Referencia, maximom Por Valor)
	Si Pilallena(pilam, cimam, maximom) Entonces 
		exitom <- Falso;
	SiNo
		cimam <- cimam + 1;
		pilam[cimam] <- elem;
		exitom <- Verdadero;
	FinSi
FinSubProceso

SubProceso SacarPila (pilasa Por Referencia, cimasa Por referencia, elesa Por Referencia, exitosa Por Referencia)
	Si PilaVacia(pilasa, cimasa) Entonces 
		exitosa <- Falso;
	SiNo
		elesa <- pilasa[cimasa]
		cimasa <- cimasa - 1;
		exitosa <- Verdadero;
	FinSi
FinSubProceso

SubProceso RecuperarPila (pilar Por Referencia, cimar Por Valor, eler Por Referencia, exitor Por Referencia)
	Si PilaVacia(pilar, cimar) Entonces 
		exitor <- Falso;
	SiNo
		eler <- pilar[cimar]
		exitor <- Verdadero;
	FinSi
FinSubProceso

Subproceso Menu
	Escribir " ";
	Escribir "Elija una de las siguientes opciones:";
	Escribir "   1 - Introducir elemento en la pila";
	Escribir "   2 - Sacar elemento de la pila"; 
	Escribir "   3 - Consultar elemento de la pila";
	Escribir "   4 - Visualizar los elementos de la pila segun entrada";
	Escribir "   5 - Visualizar los elementos de la pila segun salida";
	Escribir "   6 - Inicializar la pila";
	Escribir "   0 - Finalizar"
FinSubProceso

