Algoritmo MenuOrdenacion
	Definir vector Como Entero;
	Dimension vector[1000];
	Definir gap Como Entero;
	Dimension salto[5];
	salto[1] <- 6;
	salto[2] <- 4;
	salto[3] <- 3;
	salto[4] <- 2;
	salto[5] <- 1;
	Definir entrada, long, i, j, k, m Como Entero;
	
	Escribir "Introduzca la longitud del Array: ";
	Leer long;
	InicioArray(vector, long);
	VerArray(vector, long);
	Menu;
	Leer entrada;
	
	Mientras entrada <> 0 Hacer
		Segun entrada Hacer
			1:
				Escribir "Introduzca la longitud del Array: ";
				Leer long;
				InicioArray(vector, long);
				VerArray(vector, long);
				
			2:
				i <- 1
				Repetir
					Escribir "Pasada: ", i;
					nointercambio <- Verdadero;
					Para j <- 1 Hasta long - 1 Con Paso 1 Hacer
						Si vector[j] > vector[j+1] Entonces
							Intercambiar(vector, j, j+1);
							Escribir " Posicion: ", j, "  ", Sin Saltar;
							VerArray(vector, long);
							nointercambio <- Falso;
						FinSi
					FinPara
					i <- i + 1;
				Hasta Que nointercambio  
			3:
				i <- 1
				Repetir
					nointercambio <- Verdadero;
					Para j <- 1 Hasta long - 1 Con Paso 1 Hacer
						Si vector[j] > vector[j+1] Entonces
							Intercambiar(vector, j, j+1);
							nointercambio <- Falso;
						FinSi
					FinPara
					i <- i + 1;
				Hasta Que nointercambio 
				VerArray(vector, long);
			4:
				Para j <- long Hasta 2 Con Paso - 1 Hacer
					ElementoMayor(vector, j, may);
					Si vector[j] <> vector[may] Entonces
						Intercambiar(vector, may, j);
						Escribir " Cambio: ", may, " por ", j, "  ", Sin Saltar;
						VerArray(vector, long);
					FinSi
				FinPara
			5:
				Para j <- long Hasta 2 Con Paso - 1 Hacer
					ElementoMayor(vector, j, may);
					Si vector[j] <> vector[may] Entonces
						Intercambiar(vector, may, j);
					FinSi
				FinPara
				VerArray(vector, long);
			6:
				Para i <- 2 Hasta long Con Paso 1 Hacer
					aux <- vector[i];
					Escribir "La posicion: ", i, Sin Saltar;
					j <- i;
					Mientras j > 1 y vector[j-1] > aux Hacer
						vector[j] <- vector[j-1]
						j <- j - 1
					FinMientras;
					vector[j] <- aux;
					Escribir " pasa a la posicion: ", j, "  ", Sin Saltar;
					VerArray(vector, long);
				FinPara
			7:
				Para i <- 2 Hasta long Con Paso 1 Hacer
					aux <- vector[i];
					j <- i;
					Mientras j > 1 y vector[j-1] > aux Hacer
						vector[j] <- vector[j-1]
						j <- j - 1
					FinMientras;
					vector[j] <- aux;
				FinPara
				VerArray(vector, long);
			8:
				Para i <- 1 Hasta 5 Con Paso 1 Hacer
					gap <- salto[i]
					Para j <- 1 Hasta gap Con Paso 1 Hacer
						Para k <- (gap + j) Hasta long Con Paso gap Hacer
							aux <- vector[k];
							m <- k;
							Mientras m > gap y vector[m-gap] > aux Hacer
								vector[m] <- vector[m-gap]
								m <- m - gap
							FinMientras;
							Si m <> k Entonces
								vector[m] <- aux;
								Escribir "La posicion: ", k, Sin Saltar;
								Escribir " pasa a la posicion: ", m, "  ", Sin Saltar;
								VerArray(vector, long);
							FinSi
						FinPara
					FinPara
				FinPara
			9:
				Para i <- 1 Hasta 5 Con Paso 1 Hacer
					gap <- salto[i]
					Para j <- 1 Hasta gap Con Paso 1 Hacer
						Para k <- (gap + j) Hasta long Con Paso gap Hacer
							aux <- vector[k];
							m <- k;
							Mientras m > gap y vector[m-gap] > aux Hacer
								vector[m] <- vector[m-gap]
								m <- m - gap
							FinMientras;
							Si m <> k Entonces
								vector[m] <- aux;
							FinSi
						FinPara
					FinPara
				FinPara
				VerArray(vector, long);
			De Otro Modo:
				Escribir "OPcion incorrecta, Vuelva a introducir la opcion"
		Fin Segun
		
		Menu;
		Leer entrada;
				
	Fin Mientras
	
	
	
FinAlgoritmo

SubProceso InicioArray (vectori Por Referencia, longi Por Valor)
	Definir entrada2 Como Entero;
	Definir entok Como Logico;
	Menu2;
	Repetir
		Leer entrada2;
		entok <- Verdadero
		Segun entrada2 Hacer
			1:
				InicioArraySR(vectori, longi);
			2:
				InicioArrayCR(vectori, longi);
				
			De Otro Modo:
				entok <- Falso;
				Escribir "OPcion incorrecta, 1 o 2";
		Fin Segun
	Hasta Que entok
FinSubProceso

SubProceso InicioArraySR (vectorsr Por Referencia, longsr Por Valor)
	Para i <- 1 Hasta longsr Con Paso 1 Hacer
		Repetir
			noexiste <- Verdadero;
			num <- azar(longsr) + 1;
			Para j <- 1 Hasta (i - 1) Con Paso 1 Hacer
				Si num = vectorsr[j] Entonces
					noexiste <- Falso;
				FinSi
			FinPara
		Hasta Que noexiste
		vectorsr[i] <- num
	FinPara
FinSubProceso

SubProceso InicioArrayCR (vectorcr Por Referencia, longcr Por Valor)
	Para i <- 1 Hasta longcr Con Paso 1 Hacer
		vectorcr[i] <- azar(longcr) + 1;
	FinPara
FinSubProceso

SubProceso VerArray (vectorv Por Referencia, longv Por Valor)
	Escribir "Vector: ", Sin Saltar;
	Para i <- 1 Hasta longv Con Paso 1 Hacer
		Escribir vectorv[i], " ", Sin Saltar;
	FinPara
	Escribir " ";
FinSubProceso

SubProceso Intercambiar (vectori Por Referencia, actual Por Valor, siguiente Por Valor)
	Definir aux Como Entero;
	aux <- vectori[actual];
	vectori[actual] <- vectori[siguiente];
	vectori[siguiente] <- aux;
FinSubProceso

SubProceso ElementoMayor (vectore Por Referencia, longe Por Valor, mayore Por Referencia)
	Definir maximo Como Entero;
	mayore <- 1;
	maximo <- vectore[1]
	Para i <- 2 Hasta longe  Con Paso 1 Hacer
		Si vectore[i] > maximo Entonces
			mayore <- i;
			maximo <- vectore[i]
		FinSi
	FinPara
FinSubProceso

Subproceso Menu
	Escribir " ";
	Escribir "Elija una de las siguientes opciones:";
	Escribir "   1 - Nuevo vector"; 
	Escribir "   2 - Ordenar el vector Burbuja con seguimiento";
	Escribir "   3 - Ordenar el vector Burbuja";
	Escribir "   4 - Ordenar el vector Seleccion con seguimiento";
	Escribir "   5 - Ordenar el vector Seleccion";
	Escribir "   6 - Ordenar el vector Insercion con seguimiento";
	Escribir "   7 - Ordenar el vector Insercion";
	Escribir "   8 - Ordenar el vector Shell con seguimiento";
	Escribir "   9 - Ordenar el vector Shell";
	Escribir "   0 - Finalizar"
FinSubProceso

Subproceso Menu2
	Escribir "   1 - Sin numeros repetidos"; 
	Escribir "   2 - Con numeros repetidos";
FinSubProceso	